package com.restservice.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleRestServiceApplication {
    public static void main(final String[] args) {
        SpringApplication.run(SampleRestServiceApplication.class, args);
    }
}
