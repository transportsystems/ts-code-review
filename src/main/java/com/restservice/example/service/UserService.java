package com.restservice.example.service;

import com.restservice.example.domain.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> getList();

}
